.. _react:

========================
React front end for Lino
========================

This is the developer documentation for the ``react`` repository, which contains
the :term:`React front end` for Lino.

Source code repository: https://gitlab.com/lino-framework/react

Rendered documentation is published on two places,
https://react.lino-framework.org
and
https://lino-framework.gitlab.io/react/,
the former can be in advance of the published source code.



Content
========

.. toctree::
   :maxdepth: 1

   about/index
   guide/index
   dev/index
   changes/index
