============================
The React front end for Lino
============================

The ``lino_react`` package contains the  React front end for Lino.

Project homepage is https://gitlab.com/lino-framework/react
