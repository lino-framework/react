.. _react.guide:

==========================
Lino React Developer Guide
==========================

The ``react`` repository contains the source code for two different things:

- the :mod:`lino_react` Python package (distributed on PyPI)

- the Javascript code and npm config files needed for building the static files
  included in the :mod:`lino_react` package. Building the static files requires
  :cmd:`npm` and is described in :doc:`/guide/npm`.


.. toctree::
   :maxdepth: 1

   start
   npm
   test
   i18n
