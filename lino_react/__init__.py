# -*- coding: UTF-8 -*-
# Copyright 2018-2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
# Source documentation see https://react.lino-framework.org/dev

__version__ = '25.3.0'

srcref_url = 'https://gitlab.com/lino-framework/react/blob/master/%s'
# doc_trees = []
# intersphinx_urls = dict(docs="https://lino-framework.gitlab.io/react/")
intersphinx_urls = dict(docs="https://react.lino-framework.org/")
